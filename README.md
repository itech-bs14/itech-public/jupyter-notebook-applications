# Python Einführung

## Einführung in die Programmierung mit Python (01 - 09)

Eine schrittweise Einführung in die Syntax und Semantik von Python.

Ersteller: Jens Dittrich, [Big Data Analytics Group](https://bigdata.uni-saarland.de/), [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

Basierend auf den [Tutorials von Zhiya Zuo](https://github.com/zhiyzuo/python-tutorial).

Videos (in Deutsch) auf [YouTube](https://www.youtube.com/watch?v=1S4Cgtkxqhs&list=PLC4UZxBVGKte4XagApdryLsnIXpjZWSAn).


## OOP Grundlagen

Eine Einführung in die Objektorientierte Programmierung mit Python.

Ersteller: Michael Heinemann, [![CC-BY-NC](https://i.creativecommons.org/l/by-nc/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc/4.0/")

---

### Kurs in deutscher Sprache mit Übungen

**Einführung in Python für Nicht-Informatiker**

https://github.com/fotisj/python_intro

---

### JupyterLab via [Binder](https://mybinder.org/)

Erreichbar unter folgendem Link: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/itech-bs14%2Fitech-public%2Fjupyter-notebook-applications/master)



### JupyterLab Local

- die erforderlichen Packete installieren:  
``
$ pip install -r requirements.txt
``  
or  
``
$ pip install jupyterlab
``
- JupyteLab starten  
``
$ jupyter lab
``
